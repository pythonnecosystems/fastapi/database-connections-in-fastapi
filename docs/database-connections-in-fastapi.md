# FastAPI의 데이터베이스 연결: 종속성 주입 및 모범 사례 활용하기

## FastAPI에서 데이터베이스에 연결하기
FastAPI에서 데이터베이스에 연결하려면 일반적으로 데이터베이스와 상호 작용을 관리하기 위해 SQLAlchemy를 사용한다. 데이터베이스 연결을 설정하는 코드는 다음과 같다.

```python
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import settings

engine = create_engine(
    settings.database_url,
    pool_pre_ping=True
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
```

위의 코드에서 데이터베이스 연결을 처리하기 위해 database engine과 sessionmaker를 생성한다. 필요할 때 `get_db` 메서드를 사용하여 데이터베이스 세션을 생성할 수 있다.

FastAPI에서는 `yield`를 사용하여 하위 종속성을 생성할 수 있다. `return` 문과 달리 `yield`를 사용하면 함수가 여러 출력을 생성할 수 있으며 전체 함수를 중단하지 않는다. 이는 데이터베이스 연결과 같은 리소스를 관리해야 할 때 특히 유용하다.

이 예에서 `get_db` 함수는 `SessionLocal`을 사용하여 데이터베이스 세션을 생성한다. 이 함수는 `yield`를 사용하여 세션을 다른 함수에 대한 종속성으로 제공합니다. `finally` 블록은 예외가 발생하더라도 데이터베이스 세션이 바르게 닫히도록 한다. 이렇게 하면 주어진 시간에 하나의 데이터베이스 세션만 활성 상태로 유지된다.

## 종속성 주입
이 데이터베이스 세션을 효율적으로 사용하기 위하여 의존성 주입을 사용할 수 있다. 두 가지 방법으로 구현할 수 있다.

### 접근 방식 1: 함수 기반 종속성
FastAPI에서 데이터베이스를 연결하여 작업하는 일반적인 방법 중 하나는 의존성 주입을 사용하는 것이다. 데이터베이스 연결을 위한 종속성을 생성한 다음 라우트 핸들러에 주입하는 방법은 다음과 같다.

```python
def example_router(db: Session = Depends(get_db)):
    # Your route handler logic
    example_service(db)

def example_service(db: Session):
    # Your service logic
    create_something(db)
    
def create_something(db: Session):
    # Your database operation logic
    db.add(something)
```

이 접근 방식에서는 데이터베이스 세션을 가져오기 위한 종속성 역할을 하는 `get_db` 함수를 정의한다. 그런 다음 이 종속성을 라우트 핸들러 또는 서비스에 삽입할 수 있다.

### 접근 방식 2: 클래스 기반 종속성
또 다른 접근 방식은 클래스 내에 데이터베이스 연결 로직을 캡슐화하여 종속성으로 사용하는 것이다. 이 접근 방식은 코드 구성이 개선되고 테스트가 쉬워지는 등 여러 가지 장점이 있다.

```python
def example_router(s: Annotated[Service, Depends(Service)]):
    # Your route handler logic
    s.example_service()

class Service:
    def __init__(self, r: Annotated[Repository, Depends(Repository)]):
        self.repository = r

    def example_service(self):
        # Your service logic
        self.repository.create_something()

class Repository:
    def __init__(self, db: Annotated[Session, Depends(get_db)]):
        self.db = db

    def create_something(self):
        # Your database operation logic
        self.db.add(something)
```

이 접근 방식에서는 리포지토리 클래스에 종속되는 Service 클래스를 만들고, 이 클래스는 다시 데이터베이스 세션에 종속된다. 이 설계는 코드 구성과 테스트 가능성을 향상시킨다.

### 접근 방식 2가 더 바람직한 이유
이제 클래스 기반 종속성을 사용하는 접근 방식 2가 접근 방식 1보다 더 우수한 이유를 자세히 살펴보겠다.

- 더 나은 코드 구성: 접근 방식 2은 전용 클래스 내에서 데이터베이스 액세스를 캡슐화하여 우려 사항을 보다 깔끔하게 분리한다. 그 결과 보다 체계적이고 유지 관리하기 쉬운 코드베이스가 생성된다.
- 테스트 가능성: 클래스 기반 종속성은 테스트하기가 더 쉽다. 리포지토리 클래스를 모의 테스트하거나 테스트 더블로 대체하여 서비스 로직을 데이터베이스와 독립적으로 분리하고 테스트할 수 있다.
- 종속성 관리: 접근 방식 2에서는 종속성 관리가 더 간단해진다. 종속성이 어떻게 구성되는지 쉽게 확인하고 제어할 수 있으므로 필요할 때 종속성을 추가하거나 변경하기가 더 쉬워진다.
- 캡슐화: 클래스를 사용하면 데이터베이스 로직을 캡슐화하고 클래스 생성자와 소멸자 내에서 리소스 수명 주기를 관리하여 적절한 리소스 정리를 보장할 수 있다. 라우터는 서비스 종속성에 대해 알 필요가 없고, 서비스는 리포지토리 종속성에 대해 알 필요가 없다. 따라서 접근 방식 2가 바람직하다.

FastAPI는 종속성에 대한 타입을 지정할 수 있는 주석 기능을 제공한다. 타입을 직접 지정하는 대신 주석을 사용하여 코드 가독성과 에디터 지원을 향상시킬 수 있다.

아래 예에서는 종속성 타입을 지정하는 데 라우터_2가 선호되는 방식이다.

```python
def router_1(s: Service = Depends(Service)):
    # Your route handler logic
    s.example_service()

def router_2(s: Annotated[Service, Depends(Service)]):
    # Your route handler logic
    s.example_service()
```

## 마치며
FastAPI는 종속성 주입을 통해 데이터베이스 세션을 효율적으로 사용할 수 있는 방법을 제공한다. 함수와 클래스 기반 접근 방식 모두 유효하지만, 클래스 기반 종속성은 더 나은 코드 구성, 향상된 테스트 가능성, 간소화된 종속성 관리를 제공한다. 이러한 방식을 채택하면 FastAPI의 기능을 활용하여 강력하고 유지 관리가 쉬운 웹 어플리케이션을 쉽게 구축할 수 있다.

## 참고 문헌
- [Dependencies](https://fastapi.tiangolo.com/tutorial/dependencies/)

